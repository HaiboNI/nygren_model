// Header file for the ICs and RHS functions for the Colman 2013 model

#ifndef Nygren_1998_ODE_H
#define Nygren_1998_ODE_H

// #include "atrial_parameter.h"
#include "SingleCellParameter.hpp"
// #include <cmath>
// #include "CRN.h"


/********* The constants of the human atrail cell model********/
/*Fararday constant:96485 Coulombs/mole */
static const double Faraday = 96485.0;
static const double R       = 8314.41;    /*Maolar gas constant*/

static const double Temp    = 306.15;  /*temperature of the cell 33.6 C*/
static const double RTONF   = (306.15 * 0.08554); /*RT/F*/

/************set characteristic constants in ventricular cell*************/

static const double Vcell = 3.497E-6;
static const double Vi    =    5.884E-6;          // what is Vi ? the volume?
static const double VCa   =  5.884E-6;
static const double  Vc   =     0.136 * 5.884E-6;
static const double  Vup  =    3.969E-7;
static const double  Vrel =   4.41E-8;
static const double  Vds  =    0.02 * 5.884E-6;
/**************set constants of every ionic currents**********************/
// Capacitance for Purkinje fibre: 50*10(-6)nF
// the unit here should be 50*10(-6)uF,
// comment by haibo
static const double  Cm   = 0.000050;

/********************the parameters of stimulation ************************/

const static double  TotalTime  = 20;  /* total time_1 1.0 s*/  // maybe changed in the program, not a constant.
//  static const double  HT       = 0.00001;
//  /*integral time_1 interval 0.1 ms*/
static const double  HI         = 0.32; /*spatial interval for the medium 0.32mm*/
static const double  HI2        = (0.32 * 0.32);
static const double  DHI2       = (2.56E-2 / (0.32 * 0.32));
static const double  StiCurrent = -0.400;  /*stimulation current -0.4nA*/
static const double  EREST      = -74.2525;
// initial membrane potemtial -74.2 mV


  


void Nygren_1998_ODE_Initialise(double *state, int celltype);
double Nygren_1998_ODE(const double dt, const double Istim,  SingleCellPara &data, double *state);
double Nygren_1998_ISO_ODE (const double dt, const double Istim,  SingleCellPara & data, double *state, const double ISO);





#endif
