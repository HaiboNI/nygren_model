#include <math.h>
#include <stdio.h>

#include "stimulus.h"




/*  implementation of S1  */
double S1(double stim_start_time, double stim_current, double BCL, double current_time, double stim_duration) {
    double Istim = 0.0;
    double remain;
    double time_elapsed;

    time_elapsed = (current_time - stim_start_time) >= 0 ? (current_time - stim_start_time) : -1.0;
    remain =  fmod(time_elapsed, BCL);
    if (remain >= 0 && remain < stim_duration) {
        Istim = stim_current;
    } else {
        Istim = 0.0;
    }

    return Istim;
}

double S1S2(double stim_start_time, double stim_current, double BCL_s1, int S1_num, double BCL_s2, double current_time, double stim_duration) {
    double Istim = 0.0;
    double remain;
    double time_elapsed;

    time_elapsed = (current_time - stim_start_time) > 0 ? (current_time - stim_start_time) : 0;
    if (time_elapsed <= BCL_s1 * (S1_num - 1) + stim_duration)
    {
        remain =  fmod(time_elapsed, BCL_s1);
        if (remain >= 0 && remain <= stim_duration) {
            Istim = stim_current;
        } else {
            Istim = 0.0;
        }
    }
    else if ((time_elapsed > BCL_s1 * (S1_num - 1) + stim_duration) && (time_elapsed <= BCL_s1 * (S1_num - 1) + BCL_s2  + stim_duration) )
    {
        time_elapsed = time_elapsed - BCL_s1 * (S1_num - 1) ;
        remain =  fmod(time_elapsed, BCL_s2);
        if (remain >= 0 && remain <= stim_duration) {
            Istim = stim_current;
        } else {
            Istim = 0.0;
        };
    }
    else
    {
        Istim = 0.0;
    }
    return Istim;
}


double S1S2_num(double stim_start_time, double stim_current, double BCL_s1, int S1_num, double BCL_s2, int S2_num, double current_time, double stim_duration) {
    double Istim = 0.0;
    double remain;
    double time_elapsed;

    time_elapsed = (current_time - stim_start_time) > 0 ? (current_time - stim_start_time) : 0;
    if (time_elapsed <= BCL_s1 * (S1_num - 1) + stim_duration)
    {
        remain =  fmod(time_elapsed, BCL_s1);
        if (remain >= 0 && remain <= stim_duration) {
            Istim = stim_current;
        } else {
            Istim = 0.0;
        }
    }
    else if ((time_elapsed > BCL_s1 * (S1_num - 1) + stim_duration) && (time_elapsed <= BCL_s1 * (S1_num - 1) + BCL_s2 * S2_num + stim_duration) )
    {
        time_elapsed = time_elapsed - BCL_s1 * (S1_num - 1) ;
        remain =  fmod(time_elapsed, BCL_s2);
        if (remain >= 0 && remain <= stim_duration) {
            Istim = stim_current;
        } else {
            Istim = 0.0;
        };
    }
    else
    {
        Istim = 0.0;
    }
    return Istim;
}

double S1S2_num_stim(double stim_start_time, double BCL_s1, double stim_current_1, int S1_num,
                     double BCL_s2, double stim_current_2, int S2_num,
                     double current_time, double stim_duration) {

    double Istim = 0.0;
    double remain;
    double time_elapsed;

    time_elapsed = (current_time - stim_start_time) > 0 ? (current_time - stim_start_time) : 0;
    if (time_elapsed <= BCL_s1 * (S1_num - 1) + stim_duration)
    {
        remain =  fmod(time_elapsed, BCL_s1);
        if (remain >= 0 && remain <= stim_duration) {
            Istim = stim_current_1;
        } else {
            Istim = 0.0;
        }
    }
    else if ((time_elapsed > BCL_s1 * (S1_num - 1) + stim_duration) && (time_elapsed <= BCL_s1 * (S1_num - 1) + BCL_s2 * S2_num + stim_duration) )
    {
        time_elapsed = time_elapsed - BCL_s1 * (S1_num - 1) ;
        remain =  fmod(time_elapsed, BCL_s2);
        if (remain >= 0 && remain <= stim_duration) {
            Istim = stim_current_2;
        } else {
            Istim = 0.0;
        };
    }
    else
    {
        Istim = 0.0;
    }
    return Istim;
}

double simple_S1(double * stim_lapse, double stim_start, double stim, double BCL, double current_time, double stim_duration, double dt) {
	/*double Istim = 0.0;
    if ((* stim_lapse) >= st && (* stim_lapse) <= 5.0) Istim = 12.5; else Istim = 0.0;
    if ((* stim_lapse) > BCL) (* stim_lapse) = 0.0;
    (* stim_lapse) = (* stim_lapse) + dt;*/
    return 0;
}
