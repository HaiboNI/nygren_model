/*
 * stimulus.h
 * Haibo Ni <qiangzini@gmail.com>
 *
 * generate stimulus according to S1 or S1S2 proctol
 *
 * 
 *	Last Update: Dec 7 2014
 */

#ifndef STIMULUS_H
#define STIMULUS_H

#include <math.h>
#include <stdio.h>
// #include <iostream>
// #include <fstream>
// #include "vector"

/*  all the times are in ms */
 /* all current in pA/pF */

double S1(double stim_start, double stim, double BCL, double current_time, double stim_duration);

double S1S2(double stim_start_time, double stim_current, double BCL_s1, int S1_num, double BCL_s2, double current_time, double stim_duration);
double S1S2_num(double stim_start_time, double stim_current, double BCL_s1, int S1_num, double BCL_s2, int S2_num, double current_time, double stim_duration) ;

double S1S2_num_stim(double stim_start_time, double BCL_s1, double stim_current_1, int S1_num, 
						double BCL_s2, double stim_current_2, int S2_num,
						double current_time, double stim_duration) ;
double simple_S1(double stim_start, double stim, double BCL, double current_time, double stim_duration);


/*typedef struct
{
	double ERP;
	double BCL_s1, BCL_s2;
	double ap_amp_ratio;
	double BCL_adjust;
} ERP_measure_struct;
*/

#endif   // END OF STIMULUS.H 

