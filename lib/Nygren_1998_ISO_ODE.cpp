/*implementing ISO effect in the Nygren et al. 1998 model */




#include <math.h>
#include "Nygren_1998_ODE.h"

double Nygren_1998_ISO_ODE(const double dt, const double Istim,  SingleCellPara & data, double *state, const double ISO)
{


	int i, IT, inst, ikst;
	/* membrane potential */
	double E, E1;
	/* variables for IKr, IKs */
	double pa, pn;
	/* variables for INa */
	double m, h1, h2;
	/* varibels for Ito, Isus */
	double r, s, rs, sS;
	/* variables fo
	r ICa,L */
	double dL, fL1, fL2;
	/* variables for intra- ionic concentrations */
	double Nai, Cai, Ki;
	/* variables for extra- ionic concentrations */
	double Nac, Cao, Kc, Cads;
	/* variables for CICR in SR */
	double Caup, CaCal, Carel, CaCalse, Catrop;
	double Camg, Mgmg;
	double F1, F2;
	/* stimulation */
	// double Ipulse;

	double bar_pa, tpa, bar_n, tn, bar_r, tr, bar_s, taus, bar_rs, trs,
	       bar_ss, tss, bar_m, smx, tm, bar_h, th1, th2, bar_dl, xdl,
	       tdl, bar_fl, xfl, tfl;

	double fCa, fab, ract, rinact;

	double time_1, LE;

	double Remain;
	double BCL = 1.0;
	double time_elapsed;
	double effective_BCL, BCL_2;

	/*temparory variables to store the states, etc*/
	double sE, spa, spi, spn, sm, sh1, sh2, sr, ss, srs, sss, sdL, sfL1, sfL2,
	       sNai, sCai, sKi, sNac, sCao, sKc, sCads, sCaup, sCaCal, sCarel,
	       sCaCalse, sCatrop, sCamg, sMgmg, sF1, sF2;
	/****************************************************************/
	double ENa, EK, EKs, ECa, E0;

	double IKf, IKs, IK, IK1, Ito, Isus, IbNa,
	       Ip, INaCa, IbCa, INa, ICap, ICaL, Itot,
	       Iup, Itr, Irel, idi;

	E       = state[0] ;
	pa      = state[1] ;
	pn      = state[2] ;
	r       = state[3] ;
	s       = state[4] ;
	rs      = state[5] ;
	sS      = state[6] ;
	m       = state[7] ;
	h1      = state[8] ;
	h2      = state[9] ;
	dL      = state[10];
	fL1     = state[11];
	fL2     = state[12];
	Nac     = state[13];
	Nai     = state[14];
	Caup    = state[15];
	Carel   = state[16];
	Cai     = state[17];
	CaCal   = state[18];
	Catrop  = state[19];
	Camg    = state[20];
	Mgmg    = state[21];
	CaCalse = state[22];
	Kc      = state[23];
	Ki      = state[24];
	F1      = state[25];
	F2      = state[26];
	Cads    = state[27];
	Cao     = state[28];

	sE       = E;
	spa      = pa;
	spn      = pn;
	sr       = r;
	ss       = s;
	srs      = rs;
	sss      = sS;
	sm       = m;
	sh1      = h1;
	sh2      = h2;
	sdL      = dL;
	sfL1     = fL1;
	sfL2     = fL2;
	sNac     = Nac;
	sNai     = Nai;
	sCaup    = Caup;
	sCarel   = Carel;
	sCai     = Cai;
	sCaCal   = CaCal;
	sCatrop  = Catrop;
	sCamg    = Camg;
	sMgmg    = Mgmg;
	sCaCalse = CaCalse;
	sKc      = Kc;
	sKi      = Ki;
	sF1      = F1;
	sF2      = F2;
	sCads    = Cads;
	sCao     = Cao;





	/********   To solve the difference equation   **********/
	EK = RTONF * log(sKc / sKi);
	ENa = RTONF * log(sNac / sNai);
	ECa = (RTONF / 2.0) * log(sCao / sCai);

	IKf = 0.0005 * spa * 1.0 / (1 + exp((sE + 55.0) / 24.0)) * (sE - EK);

	IKs = (1.0+2.0*ISO)*0.001 * spn * (sE - EK);

	IK = IKf + IKs;

	E0 = sE - EK + 3.6;

	IK1 = 0.003 * pow(sKc, 0.4457) * (sE - EK)
	      / (1 + exp(1.5 * E0 / RTONF));
	// ******************************************************************
	//    IK1=1.9*IK1;
	// ******************************************************************
	Ito = 0.0075 * sr * ss * (sE - EK);
	Isus = 0.4*(1.0+2.0*ISO)*0.00275 * srs * sss * (sE - EK);

	IbNa = 0.000060599 * (sE - ENa);

	Ip = (1.0 + ISO*0.25)*0.0708253 * sKc / (sKc + 1) *
	     (pow(sNai, 1.5) / (pow(sNai, 1.5) + pow(11.0, 1.5)))
	     * ((sE + 150.0) / (sE + 200.0));

	INaCa = 0.0000374842 * (pow(sNai, 3) *
	        sCao * exp(0.450 * sE / RTONF) -
	        pow(sNac, 3) * sCai * exp(sE * (0.45 - 1) / RTONF)) /
	        (1 + 0.0003 * (sCai * pow(sNac, 3) + sCao * pow(sNai, 3)));
	/*          /(1+SCai/0.0069) */

	IbCa = 0.000078681 * (sE - ECa);

	/*      sm=1.0/(1+exp(-(sE+27.12)/8.21));  */

	if (fabs(sE) > 1.0E-4)
	{
		INa = 0.0000016 * pow(sm, 3) * (0.9 * sh1 + 0.1 * sh2) *
		      sNac * sE * Faraday / RTONF * (exp((sE - ENa) / RTONF) - 1) /
		      (exp(sE / RTONF) - 1);
	}
	else
	{
		INa = 0.0000016 * pow(sm, 3) * (0.9 * sh1 + 0.1 * sh2) *
		      sNac * Faraday * (exp((sE - ENa) / RTONF) - 1);
	}
	// /************** ****************************************/
	ICap = 0.004 * (sCai / (sCai + 0.0002));

	fCa = sCads / (sCads + 0.025);

	ICaL = (1.0+0.5*ISO)*0.00675 * sdL * (fCa * sfL1 + (1 - fCa) * sfL2) * (sE - 60.0);

	// **************************************************************
	//    ICaL=0.05*ICaL;
	// **************************************************************
	Itot = (IKf + IKs + IK1 + Ito + Isus +
		       IbNa + Ip + INaCa + IbCa + INa
		       + ICap + ICaL + Istim) / Cm;


	/*
	keep record of dvdt rate, added in by haibo
	 */

	/**********************************************************************/




	// E = sE + dt * (0 - Itot) / Cm;

	/**********************************************************************/


	/**********************************************************************/

	/**
	 *  the rest of the code is for updating all statefactors of the Ion, etc
	 */

	bar_pa = 1. / (1 + exp(-(sE + 15.0) / 6.0));
	tpa = 0.03118 + 0.21718 * exp(-pow((sE + 20.1376) / 22.1996, 2));

	pa = bar_pa + (spa - bar_pa) * exp(-dt / tpa);

	/**********************************************************************/
	bar_n = 1.0 / (1 + exp(-(sE - 19.9+10*ISO) / 12.7));
	tn = 0.7 + 0.4 * exp(-pow((sE - 20.0+10.*ISO) / 20.0, 2));

	pn = bar_n + (spn - bar_n) * exp(-dt / tn);
	/**********************************************************************/
	bar_r = 1 / (1 + exp(-(sE - 1.0) / 11.0));
	tr = (0.0035 * exp(-pow((sE / 30.0), 2))) + 0.0015;
	r = bar_r + (sr - bar_r) * exp(-dt / tr);
	/**********************************************************************/
	bar_s = 1. / (1 + exp((sE + 40.5) / 11.5));
	taus = (0.4812 * exp(-pow((sE + 52.45) / 14.97, 2))) + 0.01414;
	s = bar_s + (ss - bar_s) * exp(-dt / taus);
	/*********************************************************************/
	bar_rs = 1. / (1 + exp(-(sE + 4.3) / 8.0));
	trs = (0.009 / (1 + exp((sE + 5.0) / 12.0))) + 0.0005;
	rs = bar_rs + (srs - bar_rs) * exp(-dt / trs);
	/********************************************************************/
	bar_ss = (0.4 / (1 + exp((sE + 20.0) / 10.0))) + 0.6;
	tss = (0.047 / (1 + exp((sE + 60.0) / 10.0))) + 0.3;
	sS = bar_ss + (sss - bar_ss) * exp(-dt / tss);
	/********************************************************************/
	bar_m = 1.0 / (1 + exp(-(sE + 27.12) / 8.21));
	smx = (sE + 25.57) / 28.8;
	tm = (0.000042 * exp(-pow(smx, 2))) + 0.000024;
	m = bar_m + (sm - bar_m) * exp(-dt / tm);
	/********************************************************************/
	bar_h = 1. / (1 + exp((sE + 63.6) / 5.3));
	th1 = 0.03 / (1 + exp((sE + 35.1) / 3.2)) + 0.0003;
	th2 = 0.12 / (1 + exp((sE + 35.1) / 3.2)) + 0.003;
	h1 = bar_h + (sh1 - bar_h) * exp(-dt / th1);
	h2 = bar_h + (sh2 - bar_h) * exp(-dt / th2);
	/*******************************************************************/
	bar_dl = 1. / (1 + exp(-(sE + 9.0 + 5.0*ISO) / 5.8));
	xdl = (sE + 35.0) / 30.0;
	tdl = (0.0027 * exp(-pow(xdl, 2))) + 0.002;
	dL = bar_dl + (sdL - bar_dl) * exp(-dt / tdl);
	/*******************************************************************/
	bar_fl = 1.0 / (1 + exp((sE + 27.4 /*+3.*ISO*/) / 7.1));
	xfl = (sE + 40.0) / 14.4;
	tfl = 0.161 * exp(-pow(xfl, 2)) + 0.01;
	fL1 = bar_fl + (sfL1 - bar_fl) * exp(-dt / tfl);

	xfl = (sE + 40.0) / 14.2;
	tfl = (0.3323 * exp(-pow(xfl, 2))) + 0.0626;
	fL2 = bar_fl + (sfL2 - bar_fl) * exp(-dt / tfl);
	/*****************[Na]o********************************************/
	Nac = sNac + dt * ((130.0 - sNac) / 14.3 +
	                   (INa + IbNa + 3.*Ip + 3.*INaCa - 0.00168) / (Faraday * Vc));

	/*****************[Na]i*******************************************/

	Nai = sNai + dt * (-(3 * Ip + 3 * INaCa + IbNa + INa - 0.00168) /
	                   (Faraday * Vi));

	/*****************************************************************/
	Iup = (1.0-0.0*ISO)*2.8 * (((sCai / 0.0003) - (0.4 * 0.4 * sCaup / 0.5)) /
	             (((sCai + 0.0003) / 0.0003) + (0.4 * (sCaup + 0.5) / 0.5)));


	Irel =(1.0+0.1*ISO)* 200.0 * pow((sF2 / (sF2 + 0.25)), 2) * (sCarel - sCai);

	Itr = (sCaup - sCarel) * 2.0 * Faraday * Vrel / 0.01;
	/****************[Ca]up*******************************************/

	Caup = sCaup + dt * ((Iup - Itr) / (2.0 * Faraday * Vup));
	/****************[Ca]rel******************************************/

	Carel = sCarel + dt * ((Itr - Irel) / (2.0 * Faraday * Vrel) -
	                       31.0 * (480 * sCarel * (1 - sCaCalse) - 400 * sCaCalse));

	/****************[Ca]Cal*****************************************/

	CaCal = sCaCal + dt * ((200000.0 * sCai * (1 - sCaCal) - 476.0 * sCaCal));
	/****************[Ca]trop****************************************/

	Catrop = sCatrop + dt * ((78400.0 * sCai * (1 - sCatrop) - 392.0 * sCatrop));
	/****************[Ca]Mgtrop**************************************/

	Camg = sCamg + dt * ((200000.0 * sCai * (1 - sCamg - sMgmg) - 6.6 * sCamg));
	/**************[Mg]Mgtrop**************************************/

	Mgmg = sMgmg + dt * (2000 * 2.5 * (1 - sCamg - sMgmg) - 666.0 * sMgmg);
	/**************************************************************/
	fab = 0.08 * (78400.0 * sCai * (1 - sCatrop) - 392.0 * sCatrop)
	      + 0.16 * (200000.0 * sCai * (1 - sCamg - sMgmg) - 6.6 * sCamg)
	      + 0.045 * (200000.0 * sCai * (1 - sCaCal) - 476.0 * sCaCal);

	idi = (sCads - sCai) * 2.0 * Vds * Faraday / 0.01;
	/***************[Ca]i***********************************************/

	// Cai = sCai + dt * ((2.0 * INaCa - ICaL - ICap - IbCa - Iup + Irel)
	//                   / (2.0 * Vi * Faraday) - 0.5 * fab);


	Cai = sCai + dt * ((idi + 2.0 * INaCa - ICap - IbCa - Iup + Irel) /
	                   (2.0 * Vi * Faraday) - 1.0 * fab);   // according to the paper, corrected by haibo
	/*******************************************************************/

	CaCalse = sCaCalse + dt * ((480 * sCarel * (1 - sCaCalse) - 400.*sCaCalse));
	/***************[K]o************************************************/

	// Kc = sKc + dt * ((5.4 - sKc) / 10.0 + (2.*Ip + IKf + IKs + Ito + IK1 + Isus
	//                                     ) / (Faraday * Vc));

	// something is wrong here, thus I have just change the sign of Ip current. by haibo
	Kc = sKc + dt * ((5.4 - sKc) / 10.0 + (-2.0 * Ip + IKf + IKs + Ito + IK1 + Isus) /
	                 (Faraday * Vc));
	/***************[K]i***********************************************/

	// Ki = sKi + dt * ((-2.*Ip - IKf - IKs - Ito - Isus - IK1) / (Faraday * Vi));
	// corrected by haibo
	Ki = sKi + dt * ((2.0 * Ip - IKf - IKs - Ito - Isus - IK1) / (Faraday * Vi));
	/******************************************************************/
	ract = 203.8 * pow((sCads / (sCads + 0.003)), 4) +
	       203.8 * pow((sCai / (sCai + 0.0003)), 4);

	rinact = 33.96 + 339.6 * pow((sCai / (sCai + 0.0003)), 4);
	/******************************************************************/

	F1 = sF1 + dt * (0.815 * (1 - sF1 - sF2) - ract * sF1);
	F2 = sF2 + dt * (ract * sF1 - rinact * sF2);
	/****************[Ca]ds********************************************/

	idi = (sCads - sCai) * 2.0 * Vds * Faraday / 0.01;

	//Cads = sCads + dt * (-(ICaL + idi) / (2.0 * Vds * Faraday));

	Cads = sCads + dt * (-(ICaL + idi) / (2.0 * Vds * Faraday));

	/****************[Ca]o*********************************************/

	//Cao = sCao + dt * ((1.8 - sCao) / 24.7 + (ICaL + IbCa + ICap - 2.0 * INaCa) / (Vc * Faraday));

	Cao = sCao + dt * ((1.8 - sCao) / 24.7 + (ICaL + IbCa + ICap - 2.0 * INaCa) / (2.0 * Vc * Faraday));  // modified by haibo

	state[0] = E;
	state[1] = pa;
	state[2] = pn;
	state[3] = r;
	state[4] = s;
	state[5] = rs;
	state[6] = sS;
	state[7] = m;
	state[8] = h1;
	state[9] = h2;
	state[10] = dL;
	state[11] = fL1;
	state[12] = fL2;
	state[13] = Nac;
	state[14] = Nai;
	state[15] = Caup;
	state[16] = Carel;
	state[17] = Cai;
	state[18] = CaCal;
	state[19] = Catrop;
	state[20] = Camg;
	state[21] = Mgmg;
	state[22] = CaCalse;
	state[23] = Kc;
	state[24] = Ki;
	state[25] = F1;
	state[26] = F2;
	state[27] = Cads;
	state[28] = Cao;

	return Itot;
	// ERP_found = true;
}




