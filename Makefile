# Makefile


CXX=icpc

CFLAGS =  -Ilib -O3 -std=c++11
CXXFLAGS = -Ilib -O3 -std=c++11

LDFLAGS=  -lz 

main:$(patsubst %.cpp,%.o,$(wildcard lib/*.cpp))
main_ISO:$(patsubst %.cpp,%.o,$(wildcard lib/*.cpp))
clean:
	rm lib/*.o main


