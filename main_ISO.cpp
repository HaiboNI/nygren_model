#include <vector>
#include <zlib.h>
// #include <stdio.h>
// #include <stdlib.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <string.h>

#include "SingleCellParameter.hpp"
// #include "CRN_ODE.h"
#include "Nygren_1998_ODE.h"



int main(int argc, char const *argv[])
{
	double *state;
	state = (double *)malloc(40 * sizeof(double));
	double stim;
	double v;
	double dv;
	double BCL = 1000.0;
	// crn_con_initial_conditions_Vent(state, 1 );
	// int in_celltype = atoi(argv[1]);
	// double ABIndex = atof(argv[2]);
	int count = 0;
	double Total_time = 30000;
	int outfreq = 50;
	double t = 0.0;
	double outputTime = 0.0;
	std::ofstream output_file( "out.dat" );                      // output filename

	// TypeCell celltype    = LVEPI;
	// celltype = (TypeCell)in_celltype;                            // Type of cell  0 - 5
	double epi_mid_ratio = 1.5;                                  // EPi:MID ratio
	double time_step     = 0.01;                                 // Time step
	// TNNP_MarkovIKr_Initialise(state, celltype);

	Nygren_1998_ODE_Initialise(state, 0);
	SingleCellPara cell_para;
	cell_para.SetCellTypePara(RA);
	cell_para.SetIKurMutationPara(WT);
	cell_para.SetAFTypePara(NONE);

	Total_time = 1000 * 1000;
	for (t = 0.0; t < Total_time; t = t + time_step)
	{
		if (outputTime >= 0.0 && outputTime <= 7.0)
			// stim = -18;
			stim = -0.400; // for Nygren model only
		else stim = 0.0;
		if (outputTime > BCL) outputTime = 0.0;
		outputTime = outputTime + time_step;
		// double dv = TNNP_MarkovIKr_ODE_RV_Gradient(time_step, stim, state, ABIndex, 1.0, celltype);
		// double dv = TNNP_MarkovIKr_ODE(time_step, stim, state, ABIndex, celltype);


		// in the Nygren et al 1998 model, unit of time was second, divided by 1000.0 here!!!
		double dv = Nygren_1998_ISO_ODE(time_step / 1000.0, stim, cell_para, state, 1.0);
		state[0] += - dv * time_step/ 1000.0;
		if (count % outfreq == 0 and t > 0 * 1000)
		{
			output_file << t << ' ' << state[0] << ' ' << dv << ' ' << std::endl;
		}
		count ++;
	}
	output_file.close();
	return 0;
}
